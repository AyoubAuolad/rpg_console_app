﻿using RPG_CONSOLE_APP;
using RPG_CONSOLE_APP.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RPG_CONSOLE_APP_TESTS
{
    public class CharacterTests
    {
        [Fact]
        public void CreateCharacter_CheckIfLevelIsEqualToOne()
        {
            //Arrange
            Mage mage = new Mage("Ayoub", 1, 1, 1, 8, "Mage", 1);

            //Act
            int expected = 2;
            int actual = mage.Level;
           
            //Assert

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void LevelUp_CheckIfLevelIsEqualToTwo()
        {
            //Arrange
            Mage mage = new Mage("Ayoub", 1, 1, 1, 8, "Mage", 1);

            //Act
            int expected = 2;
            int actual = mage.LevelUp();

            //Assert

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void InstanceAttributesMage_CheckDefaultClassAttributes()
        {
            //Arrange
            Mage mage = new Mage("Ayoub", 1, 1, 1, 8, "Mage", 1);

            //Act
            int expected = 10;
            int actual = mage.Strength + mage.Dexterity + mage.Intelligence;

            //Assert

            Assert.Equal(expected, actual);

        }
        [Fact]
        public void InstanceAttributesRange_CheckDefaultClassAttributes()
        {
            //Arrange
            Ranger range = new Ranger("Ayoub", 1, 1, 7, 1, "Ranger", 1);

            //Act
            int expected = 9;
            int actual = range.Strength + range.Dexterity + range.Intelligence;

            //Assert

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void InstanceAttributesRogue_CheckDefaultClassAttributes()
        {
            //Arrange
            Rogue rogue = new Rogue("Ayoub", 1, 1, 7, 1, "Rogue", 1);

            //Act
            int expected = 9;
            int actual = rogue.Strength + rogue.Dexterity + rogue.Intelligence;

            //Assert

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void InstanceAttributesWarrior_CheckDefaultClassAttributes()
        {
            //Arrange
            Warrior warrior = new Warrior("Ayoub", 1, 5, 2, 1, "Warrior", 1);

            //Act
            int expected = 8;
            int actual = warrior.Strength + warrior.Dexterity + warrior.Intelligence;

            //Assert

            Assert.Equal(expected, actual);

        }

    }
}
